﻿using System;
using System.Reflection;
using CustomTypes;
using MyIoC;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            var targetAssembly = Assembly.LoadFrom("CustomTypes.dll");
            container.AddAssembly(targetAssembly);
            
            container.AddType(typeof(IThing), typeof(Thing));
            container.AddType(typeof(IThingParameter), typeof(ThingParameter));
            var simple = container.CreateInstance(typeof(SimplyClass));
            var simpleWithInterface = container.CreateInstance(typeof(ISimplyClassWithInterface));
            var thing = container.CreateInstance(typeof(IThing));
            
            Console.ReadKey();
        }
    }
}
