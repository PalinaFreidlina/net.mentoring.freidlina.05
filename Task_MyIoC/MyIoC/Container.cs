﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MyIoC
{
    public class Container
	{
	    private class RegisteredObject
	    {
	        public Type ConcreteType { get; }
	        public RegisteredObject(Type concreteType)
	        {
	            ConcreteType = concreteType;
	        }
	        public object CreateInstance(params object[] args)
	        {
	            object instance = Activator.CreateInstance(ConcreteType, args);
	            return instance;
	        }
	    }
        private readonly IDictionary<Type, RegisteredObject> _registeredObjects = new Dictionary<Type, RegisteredObject>();
        IEnumerable<FieldInfo> GetImportedFieldInfo(Type targetType) => targetType.GetFields().Where(field => field.IsDefined(typeof(ImportAttribute), true));
	    IEnumerable<PropertyInfo> GetImportedPropertyInfo(Type targetType) => targetType.GetProperties().Where(property => property.IsDefined(typeof(ImportAttribute), true));
	    ConstructorInfo GetImportedConstructorInfo(Type targetType) => targetType.GetConstructors().FirstOrDefault(ctor => ctor.IsDefined(typeof(ImportConstructorAttribute), true));

	    public void AddAssembly(Assembly assembly)
	    {
	        var exportTypes = assembly.ExportedTypes.Where(t => t.IsClass || t.IsInterface);
	        foreach (var type in exportTypes)
	        {
	            var export = type.GetCustomAttribute<ExportAttribute>();
	            if (export == null) continue;
	            if (export.Contract != null)
	            {
	                AddType(export.Contract, type);
	            }
	            else
	            {
	                AddType(type);
	            }
	        }

        }

	    public void AddType(Type type)
	    {
            AddType(type,type);
        }

	    public void AddType(Type type, Type baseType)
	    {
	        if (_registeredObjects.ContainsKey(type))
	            _registeredObjects.Remove(type);
	        _registeredObjects.Add(type, new RegisteredObject(baseType));

        }

	    public object CreateInstance(Type type)
	    {
	        return ResolveObject(type);
	    }
	    private object ResolveObject(Type type)
	    {
	        var registeredObject = _registeredObjects[type];
	        if (registeredObject == null)
	        {
	            throw new ArgumentOutOfRangeException($"The type {type.Name} has not been registered");
	        }
	        return GetInstance(registeredObject);
	    }
	    private object GetInstance(RegisteredObject registeredObject)
	    {
	        var parameters = ResolveConstructorParameters(registeredObject);
	        return registeredObject.CreateInstance(parameters.ToArray());
	    }
	    private IEnumerable<object> ResolveConstructorParameters(RegisteredObject registeredObject)
	    {
	        var constructorInfo = registeredObject.ConcreteType.GetConstructors().First();
	        return constructorInfo.GetParameters().Select(parameter => ResolveObject(parameter.ParameterType));
	    }

        T CreateInstance<T>()
        {
            return (T) Activator.CreateInstance(_registeredObjects[typeof(T)].ConcreteType);
        }


	    void Sample()
		{
			var container = new Container();
			container.AddAssembly(Assembly.GetExecutingAssembly());

			var customerBLL = (CustomerBLL)container.CreateInstance(typeof(CustomerBLL));
			var customerBLL2 = container.CreateInstance<CustomerBLL>();

			container.AddType(typeof(CustomerBLL));
			container.AddType(typeof(Logger));
			container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
		}
	}
}
